#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hps_0.h"

#define LW_BRIDGE_BASE 0xFF200000
#define LW_BRIDGE_SPAN 64

int fd = -1;
void *virtual_base;
volatile int *adc_ptr = NULL;
volatile int *led_ptr = NULL;

int main(void)
{
	//We are using Linux driver for physical memory access
	if((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1)
	{
		printf("/dev/mem err\n");
		return 1;
	}
	
	//Now we need to find virtual address of out AXI Brigde
	if((virtual_base = mmap(NULL, LW_BRIDGE_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, LW_BRIDGE_BASE)) == MAP_FAILED)
	{
		printf("mmap err\n");
		close(fd);
		return 1;
	}
	
	//We are accessing ADC and PIO components via AXI Bridge
	//These components are located on FPGA part of chip
	//For virtual addresses of hardware components we add their base address to bridge base address
	adc_ptr = virtual_base + ADC_0_BASE;
	led_ptr = (unsigned int *)(virtual_base + PIO_0_BASE);
	
	//Set auto update bit
	*(adc_ptr + 1) = 1;
	
	//Initialize variables that we need 
	unsigned int voltage = 0;
	unsigned short _12bit_digital_value = 0;
	unsigned short digital_value = 0;
	while(1)
	{
		//Reading digital value of input voltage from 16bit register(channel 0)
		digital_value = *(unsigned short *)(adc_ptr);
		
		//We read 16bit but we need only lower 12 bits, so we set higher 4 bits to 0
		voltage = digital_value & 0x0FFF;
		printf("Digital value: %d\n", voltage);			
		
		//Turning on led diodes
		if(voltage  <= 410)
			*led_ptr = 0x1;
		else if (voltage <= 820)
			*led_ptr = 0x3;
		else if (voltage <= 1230)
			*led_ptr = 0x7;
		else if (voltage <= 1640)
			*led_ptr = 0xF;
		else if (voltage <= 2050)
			*led_ptr = 0x1F;
		else if (voltage <= 2460)
			*led_ptr = 0x3F;
		else if (voltage <= 2870)
			*led_ptr = 0x7F;
		else if (voltage <= 3280)
			*led_ptr = 0xFF;
		else if (voltage < 3690)
			*led_ptr = 0x1FF;
		else
			*led_ptr = 0x3FF;
	}
	
	//Unmaping virtual memory
	if(munmap(virtual_base, LW_BRIDGE_SPAN) != 0)
	{
		printf("munmap err\n");
		close(fd);
		return 1;
	}
	
	printf("Kraj\n");
	close(fd);
	return 0;
}
